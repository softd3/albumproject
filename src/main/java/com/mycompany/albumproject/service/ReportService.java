/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.albumproject.service;

import com.mycompany.albumproject.dao.SaleDao;
import com.mycompany.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author Aritsu
 */
public class ReportService {
    public List<ReportSale> reportSaleByDay() {
        SaleDao saleDao = new SaleDao();
        return saleDao.getDayReport();
    }
    public List<ReportSale> reportSaleByMonth(int year) {
        SaleDao saleDao = new SaleDao();
        return saleDao.getMonthReport(year);
    }
}
